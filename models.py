# -*- coding: utf-8 -*-

try:
    import cjson as json
except ImportError:
    import json

from .fields import BaseField
import struct
from operator import attrgetter

__all__ = ['Model']

class Meta(type):
    '''Creates the metaclass for Model. The main function of this metaclass
    is to move all of fields into the _fields variable on the class.

    '''
    #def __init__(cls, name, bases, attrs):
    def __init__(cls, name, bases, attrs):
        cls._clsfields = {}
        cls._fields = cls._clsfields
        cls._fields_list = []
        cls._raw_body = ''
        for key in attrs.keys():
            value = attrs[key]
            if isinstance(value, BaseField):
                value.name = key
                cls._clsfields[key] = value
                cls._fields_list.append(value)
                delattr(cls, key)
        cls._fields_list = sorted(cls._fields_list, key=attrgetter('position'))
        cls._HEADER_KEYS = [x.name for x in cls._fields_list]

        cls._HEADER_DESCRIPTION = '<'
        for field in cls._fields_list:
            cls._HEADER_DESCRIPTION += field.type
        cls._size = struct.calcsize(cls._HEADER_DESCRIPTION)
        

class Model(object, metaclass = Meta):

    def __init__(self):
        super().__setattr__('_extra', {})
        for name in self._clsfields.keys():
            field = self._clsfields[name]
            setattr(self, name, field.default)

    def __eq__(self, other):
        eq = type(other) == type(self)
        if eq:
            for name, field in self._clsfields.iteritems():
                eq = eq and getattr(self, name) == getattr(other, name)
        return eq

    def get_size(self):
        return self._size

    @classmethod
    def from_dict(cls, D, is_json=False):
        '''This factory for :class:`Model`
        takes either a native Python dictionary or a JSON dictionary/object
        if ``is_json`` is ``True``. The dictionary passed does not need to
        contain all of the values that the Model declares.
        '''
        instance = cls()
        instance.set_data(D, is_json=is_json)
        return instance

    @classmethod
    def from_kwargs(cls, **kwargs):
        '''This factory for :class:`Model` only takes keywork arguments.
        Each key and value pair that represents a field in the :class:`Model` is
        set on the new :class:`Model` instance.

        '''
        instance = cls()
        instance.set_data(kwargs)
        return instance

    def set_data(self, data, is_json=False):
        if is_json:
            data = json.decode(data)

        if isinstance(data, self.__class__):
            data = data.to_dict()

        for name in self._clsfields.keys():
            field = self._clsfields[name]
            key = field.source or name
            if key in data:
                setattr(self, name, data.get(key))

    def __setattr__(self, key, value):
        if key in self._fields:
            field = self._fields[key]
            try:
                field.populate(value)
                super(Model, self).__setattr__(key, field.to_python())
            except Exception as e:
                raise Exception('Error setting value %s attr `%s`: %s' % (value, key, e))
        else:
            super(Model, self).__setattr__(key, value)

    def __repr__(self):
        return unicode(self.to_dict())

    def add_field(self, key, value, field):
        ''':meth:`add_field` must be used to add a field to an existing
        instance of Model. This method is required so that serialization of the
        data is possible. Data on existing fields (defined in the class) can be
        reassigned without using this method.

        '''
        self._extra[key] = field
        setattr(self, key, value)

    def to_dict(self, serial=False):
        '''A dictionary representing the the data of the class is returned.
        Native Python objects will still exist in this dictionary (for example,
        a ``datetime`` object will be returned rather than a string)
        unless ``serial`` is set to True.

        '''
        if serial:
            return dict((key, self._fields[key].to_serial(getattr(self, key)))
                        for key in self._fields.keys() if hasattr(self, key))
        else:
            return dict((key, getattr(self, key)) for key in self._fields.keys()
                       if hasattr(self, key))

    def to_json(self):
        '''Returns a representation of the model as a JSON string. This method
        relies on the :meth:`~micromodels.Model.to_dict` method.

        '''
        return json.encode(self.to_dict(serial=True))

    def loads(self, data):
        '''
        Makes restoring from JSON simplier.
        '''
        self.set_data(data, is_json=True)

    def dumps(self):
        '''
        Alias for :meth:`~micromodels.Model.to_json` method.
        '''
        return self.to_json()

    def loadb(self, raw):
        self._raw = raw
        header_tuple = struct.unpack(self._HEADER_DESCRIPTION, raw[0:self._size])
        result = dict(zip(self._HEADER_KEYS, header_tuple))

        # convert value if needed
        for field in self._fields_list:
            if field.convert_from:
                result[field.name] = field.convert_from(result[field.name])

        self.set_data(result)
        # remainder of raw string
        self._raw_body = raw[self._size:]

        # do postprocessing
        if hasattr(self, 'postprocess'):
            self.postprocess()

    def get_body(self):
        return self._raw_body

    def set_body(self, raw):
        self._raw_body = raw

    def dumpb(self, *args, **kwargs):
        # do data preparation
        if hasattr(self, 'prepare'):
            self.prepare(*args, **kwargs)

        raw_body = kwargs.pop('raw_body', None)

        values = []
        # convert value if needed
        for field in self._fields_list:
            value = getattr(self, field.name)
            if field.convert_to:
                value = field.convert_to(value)
            values.append(value)

        raw = struct.pack(self._HEADER_DESCRIPTION, *values)
        return raw + bytes(raw_body or self._raw_body, 'ascii')
