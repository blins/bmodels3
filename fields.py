# -*- coding: utf-8 -*-

UINT32  = 'I'
UINT16  = 'H'
UINT8   = 'B'
INT32   = 'i'
INT16   = 'h'
INT8    = 'b'
FLOAT   = 'f'

import time
import datetime
import types

#from mx.DateTime import DateTime, DateTimeType, DateTimeFrom


class BaseField(object):
    """Base class for all field types.

    The ``source`` parameter sets the key that will be retrieved from the source
    data. If ``source`` is not specified, the field instance will use its own
    name as the key to retrieve the value from the source data.

    """
    name = None
    creation_counter = 0
    convert_to = None
    convert_from = None

    def __init__(self, source=None, default=None, null=False, type=None, convert_to=None, convert_from=None):
        self.position = int(BaseField.creation_counter)
        BaseField.creation_counter += 1

        self.source = source
        self.default = default
        self.null = null
        if convert_to:
            self.convert_to = convert_to
        if convert_from:
            self.convert_from = convert_from
        if type:
            self.type = type

    def populate(self, data):
        """Set the value or values wrapped by this field"""
        self.data = data

    def to_python(self):
        '''After being populated, this method casts the source data into a
        Python object. The default behavior is to simply return the source
        value. Subclasses should override this method.

        '''
        return self.data

    def to_serial(self, data):
        '''Used to serialize forms back into JSON or other formats.

        This method is essentially the opposite of
        :meth:`~micromodels.fields.BaseField.to_python`. A string, boolean,
        number, dictionary, list, or tuple must be returned. Subclasses should
        override this method.

        '''
        return data


class CharField(BaseField):
    """Field to represent a simple Unicode string value."""

    empty = ''

    def to_python(self):
        """Convert the data supplied using the :meth:`populate` method to a
        Unicode string.

        """
        if self.data is None:
            if self.null:
                return None
            else:
                return self.default or self.empty
        return unicode(self.data)


class BytesField(BaseField):
    """Field to represent a simple byte string value."""

    empty = b''

    def __init__(self, *args, **kwargs):
        length = kwargs.pop('length')
        self.type = '%ds' % length
        super(BytesField, self).__init__(*args, **kwargs)

    def to_python(self):
        """Convert the data supplied using the :meth:`populate` method to a
        bytes string.

        """
        if self.data is None:
            if self.null:
                return None
            else:
                return self.default or self.empty
        return bytes(self.data)

class StrField(BytesField):
    """Field to represent a string value."""

    empty = ''
    
    def __init__(self, *args, **kwargs):
        self.encoding = kwargs.pop('encoding', 'ascii')
        super().__init__(*args, **kwargs)

    def to_python(self):
        """Convert the data supplied using the :meth:`populate` method to a
        bytes string.

        """
        if self.data is None:
            if self.null:
                return None
            else:
                return self.default or self.empty
        return str(self.data, self.encoding)


class BaseIntegerField(BaseField):
    """Field to represent an integer value"""

    empty = 0

    def to_python(self):
        """Convert the data supplied to the :meth:`populate` method to an
        integer.

        """
        if self.data is None:
            if self.null:
                return None
            else:
                return self.default or self.empty
        return int(self.data)

    def populate(self, data):
        if data is not None and not self.min <= data <= self.max:
            raise ValueError("Value '%d' not in range from %d to %d" % (data, self.min, self.max))
        self.data = data


class UnsignedInt32Field(BaseIntegerField):
    type = UINT32
    max = 0xFFFFFFFF
    min = 0


class UnsignedInt16Field(BaseIntegerField):
    type = UINT16
    max = 0xFFFF
    min = 0


class UnsignedInt8Field(BaseIntegerField):
    type = UINT8
    max = 255
    min = 0


class Int32Field(BaseIntegerField):
    type = INT32
    max = 0x7FFFFFFF
    min = -0x7FFFFFFF


class Int16Field(BaseIntegerField):
    type = INT16
    max = 32767
    min = -32768


class Int8Field(BaseIntegerField):
    type = INT8
    max = 127
    min = -128


class FloatField(BaseField):
    """Field to represent a floating point value"""

    type = FLOAT
    empty = 0.0

    def to_python(self):
        """Convert the data supplied to the :meth:`populate` method to a
        float.

        """
        if self.data is None:
            if self.null:
                return None
            else:
                return self.default or self.empty
        return float(self.data)


class FloatAsUInt32Field(FloatField):
    type = UINT32

    def __init__(self, *args, **kwargs):
        self.multiplier = float(kwargs.pop('multiplier', 1))
        super(FloatAsUInt32Field, self).__init__(*args, **kwargs)

    def convert_to(self, value):
        return int(self.multiplier * value)

    def convert_from(self, value):
        return value / self.multiplier


class DateTimeField(BaseField):
    type = UINT32

    def populate(self, data):
        if isinstance(data, int) or isinstance(data, float):
            self.data = data
        elif isinstance(data, types.NoneType):
            self.data = None
        elif isinstance(data, datetime.datetime):
            self.data = long(time.mktime(data.utctimetuple()))
#        elif isinstance(data, DateTimeType):
#            self.data = data.seconds()

    def to_serial(self, obj):
        if isinstance(obj, datetime.datetime):
            return long(time.mktime(obj.utctimetuple()))
#        elif isinstance(obj, DateTimeType):
#            return obj.gmticks()
        elif isinstance(obj, int) or isinstance(obj, float):
            return obj
        elif isinstance(obj, types.NoneType):
            return None
        raise TypeError("Given object is not of type DateTime")

    def convert_to(self, value):
        if value is not None:
            if isinstance(value, datetime.datetime):
                return long(time.mktime(value.utctimetuple()))
            elif isinstance(value, (int, float, long)):
                return long(value)
            raise TypeError("Given object is not of type datetime")
        else:
            return 0

    def convert_from(self, value):
        if value is not None:
            return value
        return self.default or self.empty
